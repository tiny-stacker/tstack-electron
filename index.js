const {app, BrowserWindow} = require('electron');
let window;

function createWindow() {
    window = new BrowserWindow({width: 800, height: 600});
    window.loadFile("index.html");
    window.on("closed", () => app.quit());
}

app.on("ready", createWindow);
